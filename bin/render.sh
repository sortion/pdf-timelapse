#!/usr/bin/env bash
: 'Generate a GIF from a git repository with pdf file versionned.'
set -euo pipefail

pdf2jpg() {
    local pdf_file
    pdf_file="$1"
    local output_dir
    output_dir="$2"
    gs -dBATCH \
        -dNOPAUSE \
        -dSAFER \
        -sDEVICE=jpeg \
        -dJPEGQ=95 \
        -r600x600 \
        -sOutputFile="${output_dir}/$(basename ${pdf_file})_%03d.jpg" \
        "${pdf_file}"
}

pdf-montage() {
    local jpg_dir
    local output_dir
    local pdf_file
    jpg_dir="$1"
    output_dir="$2"
    pdf_file="$3"
    local jpg_files
    jpg_files="${jpg_dir}/$(basename ${pdf_file})_*.jpg"
    MAGICK_CONFIGURE_PATH="/home/sortion/Documents/Projects/pdf-timelapse/etc/" \
        montage \
        -border 4 \
        -tile 4x3 \
        -geometry 595x841+11+22 \
        "${jpg_files}" \
        "${output_dir}/$(basename ${pdf_file})-montage.jpg"
}

usage() {
    echo "Usage: $0 <pdf_file> <output_dir>"
}

main() {
    pdf2jpg "$1" "$2"
    pdf-montage "$2" "$2" "$1"
}

if [ $# -ne 2 ]; then
    usage
    exit 1
fi

main "$1" "$2"
