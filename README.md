# PDF-TimeLapse

Commit your PDFs versions to a git repository and generate a time lapse video of the document changes.

## Requirements

- [Git](https://git-scm.com/)
- [ImageMagick](https://imagemagick.org/index.php)
- [Ghostscript](https://www.ghostscript.com/)

## Usage

```bash
./bin/render.sh <path-to-pdf-file> <path-to-output-dir>
```

## Known issues

ImageMagick could complain about the memory usage of the `montage` command. One could modify the `policy.xml` file to allow more memory usage ^[See <https://github.com/ImageMagick/ImageMagick/issues/396>].

## References

- [Using Ghostscript to convert multi-page PDF into single JPG? - Superuser](https://superuser.com/questions/168444/using-ghostscript-to-convert-multi-page-pdf-into-single-jpg)
